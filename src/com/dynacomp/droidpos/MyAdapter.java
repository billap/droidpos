package com.dynacomp.droidpos;

import java.util.ArrayList;

import android.content.ClipData.Item;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class MyAdapter extends ArrayAdapter<Items> {
 
        private final Context context;
        private final ArrayList<Items> itemsArrayList;
 
        public MyAdapter(Context context, ArrayList<Items> itemsArrayList) {
 
            super(context, R.layout.list_item, itemsArrayList);
 
            this.context = context;
            this.itemsArrayList = itemsArrayList;
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
 
            // 1. Create inflater 
            LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
            // 2. Get rowView from inflater
            View rowView = inflater.inflate(R.layout.list_item, parent, false);
 
            // 3. Get the two text view from the rowView
            TextView pelatisView = (TextView) rowView.findViewById(R.id.pelatis);
            TextView addressView = (TextView) rowView.findViewById(R.id.address);
            TextView telephoneView = (TextView) rowView.findViewById(R.id.telephone);
            TextView AFMView = (TextView) rowView.findViewById(R.id.AFM);
           
            // 4. Set the text for textView 
            pelatisView.setText(itemsArrayList.get(position).getpelatis());
            addressView.setText(itemsArrayList.get(position).getaddress());
            telephoneView.setText(itemsArrayList.get(position).gettelephone());
            AFMView.setText(itemsArrayList.get(position).getAFM());
            
 
            // 5. retrn rowView
            return rowView;
        }
}