package com.dynacomp.droidpos;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ListView;

public class Dashboard extends Activity {

	MyAdapter myadapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM); 
	    getActionBar().setCustomView(R.layout.action_bar);
		setContentView(R.layout.dashboard);

		myadapter = new MyAdapter(Dashboard.this, generateData());
		ListView listView = (ListView) findViewById(R.id.list_view);
		listView.setAdapter(myadapter);

		EditText editText = (EditText) findViewById(R.id.inputSearch);
		editText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				Dashboard.this.myadapter.getFilter().filter(s);  
				filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}

	private void filter(CharSequence str){
		myadapter.clear();
		myadapter.notifyDataSetChanged();
		System.out.println("STR "+str);
		for (Items item : generateData()) {
			if(item.getpelatis().contains(str) || item.getaddress().contains(str) || item.getAFM().contains(str)||item.gettelephone().contains(str))
				myadapter.add(item);
			myadapter.notifyDataSetChanged();
		}
		myadapter.notifyDataSetChanged();
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	
	private ArrayList<Items> generateData(){
        ArrayList<Items> items = new ArrayList<Items>();
        
        items.add(new Items("�������","ADDRESS1,","2610 236589","999999999",R.drawable.arrow));
        items.add(new Items("pelatis2","ADDRESS2,","2610 236589","999999999",R.drawable.arrow));
        items.add(new Items("pelatis3","ADDRESS3,","2610 236589","999999999",R.drawable.arrow));
        items.add(new Items("pelatis4","ADDRESS4,","2610 236589","999999999",R.drawable.arrow));
        items.add(new Items("pelatis5","ADDRESS5,","2610 236589","999999999",R.drawable.arrow));
        items.add(new Items("pelatis6","ADDRESS6,","2610 236589","999999999",R.drawable.arrow));
 
        return items;
    }


}