package com.dynacomp.droidpos;

import android.widget.ImageView;



public class Items{
	 
    private String pelatis;
    private String address;
    private String telephone;
    private String AFM;
    private int image;

    // constructor. 
    
    public Items(String pelatis) {
        this(pelatis, null,null,null, (Integer) null);
 
    }
    
    
    public Items(String pelatis, String address,String telephone,String AFM,int image) {
        super();
        this.pelatis = pelatis;
        this.address = address;
        this.telephone = telephone;
        this.AFM = AFM;
        this.image=image;
   
    }
    
    // getters and setters. 
    public String getpelatis() {
        return pelatis;
    }
    public void setpelatis (String pelatis) {
        pelatis = pelatis;
    }
    
    public String getaddress() {
        return address;
    }
    public void setaddress (String address) {
        address = address;
    }
    
    public String gettelephone() {
        return telephone;
    }
    public void settelephone (String telephone) {
        telephone = telephone;
    }
    
    public String getAFM() {
        return AFM;
    }
    public void setAFM (String AFM) {
        AFM = AFM;
    }
    

    
    public int getimage() {
        return image;
    }
    public void setimage (int image) {
        image = image;
    }
    
   
}