package com.dynacomp.droidpos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {
    protected static final String URL = null;
	Button btnLogin;
    EditText inputUser;
    EditText inputPass;
    TextView loginErrorMsg;
 
    // JSON Response node names
    private static String KEY_SUCCESS;
    private static String KEY_ERROR = "error";
    private static String KEY_ERROR_MSG = "error_msg";
    private static String KEY_ID = "id";
    private static String KEY_USER = "name";
    private static String KEY_PASS = "pass";
    private static String error = "����� ����� ������ � ������� ���������!";
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        // Importing all assets like buttons, text fields
        inputUser = (EditText) findViewById(R.id.loginUser);
        inputPass = (EditText) findViewById(R.id.loginPass);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        
       
        
        // Login button Click Event
        JSONObject json = new JSONObject();
        HttpURLConnection connection;
	   	 String line=null;
        btnLogin.setOnClickListener(new View.OnClickListener() {
        	
            public void onClick(View view) {
            	
            	
                final String user = inputUser.getText().toString();
                final String pass = inputPass.getText().toString();
                //send user and password to the web service
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy); 
                int response = 0;
				try {
					
					 int responseCode = 0;
					 
					 loginErrorMsg.post(new Runnable() {

				              @Override
				              public void run() {
				            	
				      try
				       {	 
				    	  
				    	  HttpClient Client = new DefaultHttpClient();
				     	 int responseCode = 0;
				     	
				    	  URL url = new URL("http://dynacomp2.eu/webs/droidpos/getete.php?login="+user+"&password="+pass+"");
				    	  Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("frodo.dynacomp.gr", 3128));
				    	  final HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
				          connection.setDoInput(true);
				          ArrayList<String> listOfSomething = null;
				    	  BufferedReader reader = null;
				    	  
				    	  try {
				    	    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				    	    String line = "";
				    	    while ((line = reader.readLine()) != null) {	
				    	      if (line.equals("[]"))
				    	      {
				    	    	  
				    	    	  loginErrorMsg.setVisibility(View.VISIBLE);
				    	    	  System.out.println("mpika");
				    	    	  System.out.println("AUTHEDICATION Failed!");
				    	    	  break;
				    	    	
				    	      }
				    	      else
				    	      {
				    	    	  
				    	    	  System.out.println("AUTHEDICATION OK!");
				    	    	  
				    	    	  Intent intent = new Intent(LoginActivity.this, Dashboard.class);
				    	          startActivity(intent);
				    	          finish();
				    	    	  break;
				
				    	      }
				    	    }
				    	  } catch (IOException e) {
				    	    e.printStackTrace();
				    	  } finally {
				    	    if (reader != null) {
				    	      try {
				    	        reader.close();
				    	      } catch (IOException e) {
				    	        e.printStackTrace();
				    	        }
				 
				    	    }
				          
				    	  }
				       } 
				      catch(Exception ex)
				         {
				    	  Log.e("RestService","Error!", ex);
				        
				          }
				              }
				          });
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
               
              
            }
        });
 
    }
}